package com.example.artovax.widoki;

import android.nfc.Tag;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class WidokiActivity extends ActionBarActivity {

    private static final String TAG = WidokiActivity.class.getName();


    private EditText mEditText;
    private TextView mTextField;
    private Button mButton;
    private LinearLayout mLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

       if(savedInstanceState == null){
           //pierwsze otwarcie
       }else{
           //obrót ekranu po zmianie jezyka
           //po restarcie telefonu
       }
      setContentView(R.layout.activity_widoki); //odwolaj się do widoku o nazwie activity_widoki
        //findViewBy musi być koniecznie po setContentView
        mEditText = (EditText) findViewById(R.id.pole_tekstowe);
       // mTextField = (TextView) findViewById(R.id.label);
        mButton = (Button) findViewById(R.id.przycisk);
        mLinear = findView(R.id.teksty_kontener);
        //widokiZKodu();
    }

    //czyli kodzenie w javie zamiast xml
    public void widokiZKodu(){
        mLinear = new LinearLayout(this);
        mLinear.setGravity(Gravity.CENTER_HORIZONTAL);
        mLinear.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT);

        mLinear.setLayoutParams(mParams);

        LinearLayout.LayoutParams mKontrolkaParams = new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        mEditText = new EditText(this);
        mEditText.setLayoutParams(mKontrolkaParams);
        mEditText.setSingleLine(true);
        mEditText.setHint("podaj tekst");

        mTextField = new TextView(this);
        mTextField.setLayoutParams(mKontrolkaParams);

        LinearLayout.LayoutParams mGuzikParams =new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        mButton=new Button(this);
        mButton.setLayoutParams(mGuzikParams);
        mButton.setText("gotowe");
        mButton.setOnClickListener( new View.OnClickListener(){
            @Override
             public void  onClick(View view){
                klikPrzycisk(view);
            }
        });

        mLinear.addView(mEditText);
        mLinear.addView(mTextField);
        mLinear.addView(mButton);

 //       ((RelativeLayout)findViewById(R.id.kontener)).addView(mLinear);

        setContentView(mLinear); // po usunieciu setContentView(R.layout.activity_widoki);  z OnCreate i tak się to uruchomi(program) czyli
        // można napisać apkę bez xml-a
    }

    public void klikPrzycisk(View mView){

        LinearLayout.LayoutParams mKontrolkaParams =new LinearLayout.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        String text = mEditText.getText().toString();

       // String resString = getResources().getString(R.string.imie_hint);// pokazowe,- odwołane siępo Ri pobranie wartosci

        Log.d(TAG, text !=null ? "tekst ma dlugosc: " + text.length():"Null!"); //wtf - co jest zle :P
        if(text==null || text.length()<3){
            //wyswietl, że za krótki teks
            Toast.makeText(this, "wpisałes za krótki tekst",Toast.LENGTH_LONG).show();
            return;
        }
        mEditText.setText("");//czyscimy pole edycyjne
        //mTextField.setText(text);
        TextView mNowePole = new TextView(this);
        mNowePole.setLayoutParams(mKontrolkaParams);
        mNowePole.setText(text);
        mLinear.addView(mNowePole);
    }

    public <T extends View> T findView(int indentyfikator) {
        return (T) findViewById(indentyfikator);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_widoki, menu);
        return true;
    }
    protected void wyczyscLinie() {
        List<View> mKolekacjaDoUsniecia = new ArrayList<View>();
        for(int i = 0; i < mLinear.getChildCount();i++) {
            View mChild = mLinear.getChildAt(i);
            mKolekacjaDoUsniecia.add(mChild);
        }
        for(View mChild : mKolekacjaDoUsniecia){
            if(mChild.getClass().equals(TextView.class)){
                if(mChild instanceof TextView && !(mChild instanceof  EditText) && !(mChild instanceof  Button)){
                   mLinear.removeView(mChild);
                }
            }
        }

  /*          //if(mChild.getClass().equals(TextView.class))
            if(mChild instanceof TextView && !(mChild instanceof EditText)&& !(mChild instanceof ))
            {
                mLinear.removeViewAt(i);
            }
        }*/

    }
    protected void wyczyscOstatni() {
        View mChild = mLinear.getChildAt(mLinear.getChildCount() - 1);
        if (mChild instanceof TextView && !(mChild instanceof EditText) &&
                !(mChild instanceof Button)) {
            mLinear.removeView(mChild);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.usun_wszystkie) {
            wyczyscLinie();
            return true;
        }
        if (id == R.id.usun_ostatni) {
            wyczyscOstatni();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
